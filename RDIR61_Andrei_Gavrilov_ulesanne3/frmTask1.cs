﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RDIR61_Andrei_Gavrilov_ulesanne3
{
    public enum TimerStatus { NotSet, Working, Stopped, Passed};

    public partial class frmTask1 : Form
    {
        public DateTime currentDateTime;
        public DateTime selectedDateTime;
        public TimeSpan dateDifference;
        public Boolean isWorking=false;
        public TimerStatus status=TimerStatus.NotSet;

        public frmTask1()
        {
            InitializeComponent();
            UpdateCurrentDateTime();
            dtpDate.Value = currentDateTime;
            dtpTime.Value = currentDateTime;

            timer1.Start();
        }

        private void btnAction_Click(object sender, EventArgs e)
        {
            if (status.Equals(TimerStatus.NotSet) || status.Equals(TimerStatus.Stopped) || status.Equals(TimerStatus.Passed))
            {
                StartResumeTimer();
            }
            else if (status.Equals(TimerStatus.Working))
            {
                StopTimer();
            }
        }

        private void StartResumeTimer()
        {
            selectedDateTime = dtpDate.Value.Date + dtpTime.Value.TimeOfDay;
            if (selectedDateTime <= currentDateTime)
            {
                lblMessage.Text = "Invalid value!";
            }
            else
            {
                
                UpdateResult();
                lblResultValue.BackColor = SystemColors.Control;
                dtpDate.Enabled = false;
                dtpTime.Enabled = false;
                btnAction.Text = "Stop";
                status = TimerStatus.Working;
                UpdateMessage();
            }
        }
        private void StopTimer()
        {
            if (status.Equals(TimerStatus.Working))
            {
                status = TimerStatus.Stopped;
            }
            UpdateMessage();
            dtpDate.Enabled = true;
            dtpTime.Enabled = true;
            if (status.Equals(TimerStatus.Stopped))
            {
                btnAction.Text = "Resume";
            }
            else if (status.Equals(TimerStatus.Passed))
            {
                btnAction.Text = "Start";
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            UpdateCurrentDateTime();
            if (status.Equals(TimerStatus.Working))
            {
                UpdateResult();
            }
        }
        private void UpdateResult()
        {
            dateDifference = (this.selectedDateTime - this.currentDateTime).StripMilliseconds();
            lblResultValue.Text = dateDifference.ToString();
            if (dateDifference.TotalSeconds<=0)
            {
                status = TimerStatus.Passed;
                lblResultValue.BackColor = Color.DarkRed;
                StopTimer();
            }
        }
        private void UpdateCurrentDateTime()
        {
            this.currentDateTime = DateTime.Now.StripMilliseconds();
            lblCurrentDate.Text = currentDateTime.ToString("dd-MM-yyyy"); lblCurrentTime.Text = currentDateTime.ToString("HH:mm:ss");
        }

        private void UpdateMessage()
        {
            if (status.Equals(TimerStatus.Working))
            {
                lblMessage.Text = "Timer started";
            }
            else if (status.Equals(TimerStatus.Stopped))
            {
                lblMessage.Text = "Timer stopped";
            }
            else if (status.Equals(TimerStatus.Passed))
            {
                lblMessage.Text = "Timer stopped";
            }
        }

        private void lblSet_Click(object sender, EventArgs e)
        {

        }
    }
}
