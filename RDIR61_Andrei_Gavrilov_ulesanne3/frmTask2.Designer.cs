﻿namespace RDIR61_Andrei_Gavrilov_ulesanne3
{
    partial class frmTask2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTask2));
            this.gbScheme = new System.Windows.Forms.GroupBox();
            this.dbtnLighting = new RDIR61_Andrei_Gavrilov_ulesanne3.DeviceButton();
            this.dbtnRadiator = new RDIR61_Andrei_Gavrilov_ulesanne3.DeviceButton();
            this.dbtnFridge = new RDIR61_Andrei_Gavrilov_ulesanne3.DeviceButton();
            this.dbtnCoffee = new RDIR61_Andrei_Gavrilov_ulesanne3.DeviceButton();
            this.dbtnHeater = new RDIR61_Andrei_Gavrilov_ulesanne3.DeviceButton();
            this.lblTotal = new System.Windows.Forms.Label();
            this.gbTimeControl = new System.Windows.Forms.GroupBox();
            this.tbSetSpeed = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.tbCustomSpeed = new System.Windows.Forms.TextBox();
            this.lblSpeed = new System.Windows.Forms.Label();
            this.tbTime = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblTimeLabel = new System.Windows.Forms.Label();
            this.gbAutomation = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpLighting = new System.Windows.Forms.DateTimePicker();
            this.dtpRadiator = new System.Windows.Forms.DateTimePicker();
            this.dtpCoffee = new System.Windows.Forms.DateTimePicker();
            this.dtpFridge = new System.Windows.Forms.DateTimePicker();
            this.dtpHeater = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.toolTipScheme = new System.Windows.Forms.ToolTip(this.components);
            this.timerMain = new System.Windows.Forms.Timer(this.components);
            this.Device = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Consumption = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.DeviceName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeviceCurrentConsumption = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeviceIsOn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.gbConsumption = new System.Windows.Forms.GroupBox();
            this.lblAllTimeTotal = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.gbScheme.SuspendLayout();
            this.gbTimeControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbTime)).BeginInit();
            this.gbAutomation.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.gbConsumption.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbScheme
            // 
            this.gbScheme.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("gbScheme.BackgroundImage")));
            this.gbScheme.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.gbScheme.Controls.Add(this.dbtnLighting);
            this.gbScheme.Controls.Add(this.dbtnRadiator);
            this.gbScheme.Controls.Add(this.dbtnFridge);
            this.gbScheme.Controls.Add(this.dbtnCoffee);
            this.gbScheme.Controls.Add(this.dbtnHeater);
            this.gbScheme.Location = new System.Drawing.Point(12, 12);
            this.gbScheme.Name = "gbScheme";
            this.gbScheme.Size = new System.Drawing.Size(780, 536);
            this.gbScheme.TabIndex = 0;
            this.gbScheme.TabStop = false;
            this.gbScheme.Text = "Sauna scheme";
            // 
            // dbtnLighting
            // 
            this.dbtnLighting.BackColor = System.Drawing.Color.DarkRed;
            this.dbtnLighting.Device = null;
            this.dbtnLighting.Location = new System.Drawing.Point(114, 406);
            this.dbtnLighting.Name = "dbtnLighting";
            this.dbtnLighting.Size = new System.Drawing.Size(45, 21);
            this.dbtnLighting.TabIndex = 7;
            this.dbtnLighting.UseVisualStyleBackColor = false;
            this.dbtnLighting.Click += new System.EventHandler(this.DeviceButton_Click);
            // 
            // dbtnRadiator
            // 
            this.dbtnRadiator.BackColor = System.Drawing.Color.DarkRed;
            this.dbtnRadiator.Device = null;
            this.dbtnRadiator.Location = new System.Drawing.Point(106, 93);
            this.dbtnRadiator.Name = "dbtnRadiator";
            this.dbtnRadiator.Size = new System.Drawing.Size(248, 14);
            this.dbtnRadiator.TabIndex = 7;
            this.dbtnRadiator.UseVisualStyleBackColor = false;
            this.dbtnRadiator.Click += new System.EventHandler(this.DeviceButton_Click);
            // 
            // dbtnFridge
            // 
            this.dbtnFridge.BackColor = System.Drawing.Color.DarkRed;
            this.dbtnFridge.Device = null;
            this.dbtnFridge.Location = new System.Drawing.Point(357, 211);
            this.dbtnFridge.Name = "dbtnFridge";
            this.dbtnFridge.Size = new System.Drawing.Size(75, 101);
            this.dbtnFridge.TabIndex = 7;
            this.dbtnFridge.UseVisualStyleBackColor = false;
            this.dbtnFridge.Click += new System.EventHandler(this.DeviceButton_Click);
            // 
            // dbtnCoffee
            // 
            this.dbtnCoffee.BackColor = System.Drawing.Color.DarkRed;
            this.dbtnCoffee.Device = null;
            this.dbtnCoffee.Location = new System.Drawing.Point(357, 93);
            this.dbtnCoffee.Name = "dbtnCoffee";
            this.dbtnCoffee.Size = new System.Drawing.Size(75, 112);
            this.dbtnCoffee.TabIndex = 7;
            this.dbtnCoffee.UseVisualStyleBackColor = false;
            this.dbtnCoffee.Click += new System.EventHandler(this.DeviceButton_Click);
            // 
            // dbtnHeater
            // 
            this.dbtnHeater.BackColor = System.Drawing.Color.DarkRed;
            this.dbtnHeater.Device = null;
            this.dbtnHeater.Location = new System.Drawing.Point(571, 93);
            this.dbtnHeater.Name = "dbtnHeater";
            this.dbtnHeater.Size = new System.Drawing.Size(75, 335);
            this.dbtnHeater.TabIndex = 7;
            this.dbtnHeater.UseVisualStyleBackColor = false;
            this.dbtnHeater.Click += new System.EventHandler(this.DeviceButton_Click);
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(139, 160);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(14, 15);
            this.lblTotal.TabIndex = 8;
            this.lblTotal.Text = "0";
            // 
            // gbTimeControl
            // 
            this.gbTimeControl.Controls.Add(this.tbSetSpeed);
            this.gbTimeControl.Controls.Add(this.label8);
            this.gbTimeControl.Controls.Add(this.tbCustomSpeed);
            this.gbTimeControl.Controls.Add(this.lblSpeed);
            this.gbTimeControl.Controls.Add(this.tbTime);
            this.gbTimeControl.Controls.Add(this.label1);
            this.gbTimeControl.Controls.Add(this.lblTime);
            this.gbTimeControl.Controls.Add(this.lblTimeLabel);
            this.gbTimeControl.Location = new System.Drawing.Point(799, 13);
            this.gbTimeControl.Name = "gbTimeControl";
            this.gbTimeControl.Size = new System.Drawing.Size(381, 113);
            this.gbTimeControl.TabIndex = 1;
            this.gbTimeControl.TabStop = false;
            this.gbTimeControl.Text = "Time control";
            // 
            // tbSetSpeed
            // 
            this.tbSetSpeed.Location = new System.Drawing.Point(215, 80);
            this.tbSetSpeed.Name = "tbSetSpeed";
            this.tbSetSpeed.Size = new System.Drawing.Size(54, 24);
            this.tbSetSpeed.TabIndex = 8;
            this.tbSetSpeed.Text = "Set";
            this.tbSetSpeed.UseVisualStyleBackColor = true;
            this.tbSetSpeed.Click += new System.EventHandler(this.tbSetSpeed_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 80);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(95, 16);
            this.label8.TabIndex = 7;
            this.label8.Text = "Custom speed";
            // 
            // tbCustomSpeed
            // 
            this.tbCustomSpeed.Location = new System.Drawing.Point(116, 80);
            this.tbCustomSpeed.Name = "tbCustomSpeed";
            this.tbCustomSpeed.Size = new System.Drawing.Size(76, 20);
            this.tbCustomSpeed.TabIndex = 6;
            // 
            // lblSpeed
            // 
            this.lblSpeed.AutoSize = true;
            this.lblSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSpeed.Location = new System.Drawing.Point(76, 45);
            this.lblSpeed.Name = "lblSpeed";
            this.lblSpeed.Size = new System.Drawing.Size(40, 16);
            this.lblSpeed.TabIndex = 5;
            this.lblSpeed.Text = "1 m/s";
            // 
            // tbTime
            // 
            this.tbTime.Location = new System.Drawing.Point(142, 39);
            this.tbTime.Maximum = 3;
            this.tbTime.Name = "tbTime";
            this.tbTime.Size = new System.Drawing.Size(127, 45);
            this.tbTime.TabIndex = 4;
            this.tbTime.ValueChanged += new System.EventHandler(this.tbTime_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Speed";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.Location = new System.Drawing.Point(76, 16);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(43, 16);
            this.lblTime.TabIndex = 1;
            this.lblTime.Text = "\"time\"";
            // 
            // lblTimeLabel
            // 
            this.lblTimeLabel.AutoSize = true;
            this.lblTimeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimeLabel.Location = new System.Drawing.Point(6, 16);
            this.lblTimeLabel.Name = "lblTimeLabel";
            this.lblTimeLabel.Size = new System.Drawing.Size(42, 16);
            this.lblTimeLabel.TabIndex = 0;
            this.lblTimeLabel.Text = "Time:";
            // 
            // gbAutomation
            // 
            this.gbAutomation.Controls.Add(this.tableLayoutPanel1);
            this.gbAutomation.Location = new System.Drawing.Point(799, 132);
            this.gbAutomation.Name = "gbAutomation";
            this.gbAutomation.Size = new System.Drawing.Size(381, 183);
            this.gbAutomation.TabIndex = 2;
            this.gbAutomation.TabStop = false;
            this.gbAutomation.Text = "Automation";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60.10638F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 39.89362F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.dtpLighting, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.dtpRadiator, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.dtpCoffee, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.dtpFridge, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.dtpHeater, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label9, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 19);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(365, 152);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(3, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 16);
            this.label4.TabIndex = 7;
            this.label4.Text = "Lighting";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(3, 104);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 16);
            this.label6.TabIndex = 5;
            this.label6.Text = "Radiator ";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(3, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 16);
            this.label5.TabIndex = 5;
            this.label5.Text = "Coffee";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(3, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 16);
            this.label3.TabIndex = 5;
            this.label3.Text = "Fridge";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(3, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Heater";
            // 
            // dtpLighting
            // 
            this.dtpLighting.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dtpLighting.CustomFormat = "HH:mm";
            this.dtpLighting.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtpLighting.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpLighting.Location = new System.Drawing.Point(222, 128);
            this.dtpLighting.Name = "dtpLighting";
            this.dtpLighting.ShowUpDown = true;
            this.dtpLighting.Size = new System.Drawing.Size(57, 22);
            this.dtpLighting.TabIndex = 6;
            this.dtpLighting.Value = new System.DateTime(2018, 4, 13, 17, 30, 0, 0);
            // 
            // dtpRadiator
            // 
            this.dtpRadiator.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dtpRadiator.CustomFormat = "HH:mm";
            this.dtpRadiator.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtpRadiator.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpRadiator.Location = new System.Drawing.Point(222, 103);
            this.dtpRadiator.Name = "dtpRadiator";
            this.dtpRadiator.ShowUpDown = true;
            this.dtpRadiator.Size = new System.Drawing.Size(57, 22);
            this.dtpRadiator.TabIndex = 6;
            this.dtpRadiator.Value = new System.DateTime(2018, 4, 13, 14, 0, 0, 0);
            // 
            // dtpCoffee
            // 
            this.dtpCoffee.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dtpCoffee.CustomFormat = "HH:mm";
            this.dtpCoffee.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtpCoffee.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCoffee.Location = new System.Drawing.Point(222, 78);
            this.dtpCoffee.Name = "dtpCoffee";
            this.dtpCoffee.ShowUpDown = true;
            this.dtpCoffee.Size = new System.Drawing.Size(57, 22);
            this.dtpCoffee.TabIndex = 6;
            this.dtpCoffee.Value = new System.DateTime(2018, 4, 13, 17, 15, 0, 0);
            // 
            // dtpFridge
            // 
            this.dtpFridge.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dtpFridge.CustomFormat = "HH:mm";
            this.dtpFridge.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtpFridge.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFridge.Location = new System.Drawing.Point(222, 53);
            this.dtpFridge.Name = "dtpFridge";
            this.dtpFridge.ShowUpDown = true;
            this.dtpFridge.Size = new System.Drawing.Size(57, 22);
            this.dtpFridge.TabIndex = 6;
            this.dtpFridge.Value = new System.DateTime(2018, 4, 13, 12, 0, 0, 0);
            // 
            // dtpHeater
            // 
            this.dtpHeater.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dtpHeater.CustomFormat = "HH:mm";
            this.dtpHeater.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtpHeater.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpHeater.Location = new System.Drawing.Point(222, 28);
            this.dtpHeater.Name = "dtpHeater";
            this.dtpHeater.ShowUpDown = true;
            this.dtpHeater.Size = new System.Drawing.Size(57, 22);
            this.dtpHeater.TabIndex = 2;
            this.dtpHeater.Value = new System.DateTime(2018, 4, 13, 21, 0, 0, 0);
            this.dtpHeater.ValueChanged += new System.EventHandler(this.dtpHeater_ValueChanged);
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(3, 4);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 16);
            this.label7.TabIndex = 4;
            this.label7.Text = "Device";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(222, 4);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 16);
            this.label9.TabIndex = 4;
            this.label9.Text = "Time";
            // 
            // timerMain
            // 
            this.timerMain.Interval = 1000;
            this.timerMain.Tick += new System.EventHandler(this.timerMain_Tick);
            // 
            // Device
            // 
            this.Device.HeaderText = "Name";
            this.Device.Name = "Device";
            this.Device.Width = 60;
            // 
            // Consumption
            // 
            this.Consumption.HeaderText = "Cons.";
            this.Consumption.Name = "Consumption";
            this.Consumption.Width = 45;
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataSource = typeof(RDIR61_Andrei_Gavrilov_ulesanne3.Device);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DeviceName,
            this.DeviceCurrentConsumption,
            this.DeviceIsOn});
            this.dataGridView1.Location = new System.Drawing.Point(6, 19);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(369, 128);
            this.dataGridView1.TabIndex = 3;
            // 
            // DeviceName
            // 
            this.DeviceName.HeaderText = "Name";
            this.DeviceName.Name = "DeviceName";
            this.DeviceName.ReadOnly = true;
            // 
            // DeviceCurrentConsumption
            // 
            this.DeviceCurrentConsumption.HeaderText = "CC";
            this.DeviceCurrentConsumption.Name = "DeviceCurrentConsumption";
            this.DeviceCurrentConsumption.ReadOnly = true;
            this.DeviceCurrentConsumption.ToolTipText = "Current consumption";
            // 
            // DeviceIsOn
            // 
            this.DeviceIsOn.HeaderText = "Is ON?";
            this.DeviceIsOn.Name = "DeviceIsOn";
            this.DeviceIsOn.ReadOnly = true;
            this.DeviceIsOn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // gbConsumption
            // 
            this.gbConsumption.Controls.Add(this.lblAllTimeTotal);
            this.gbConsumption.Controls.Add(this.lblTotal);
            this.gbConsumption.Controls.Add(this.dataGridView1);
            this.gbConsumption.Controls.Add(this.label13);
            this.gbConsumption.Controls.Add(this.label11);
            this.gbConsumption.Controls.Add(this.label12);
            this.gbConsumption.Location = new System.Drawing.Point(799, 321);
            this.gbConsumption.Name = "gbConsumption";
            this.gbConsumption.Size = new System.Drawing.Size(381, 227);
            this.gbConsumption.TabIndex = 4;
            this.gbConsumption.TabStop = false;
            this.gbConsumption.Text = "Consumption";
            // 
            // lblAllTimeTotal
            // 
            this.lblAllTimeTotal.AutoSize = true;
            this.lblAllTimeTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAllTimeTotal.Location = new System.Drawing.Point(139, 189);
            this.lblAllTimeTotal.Name = "lblAllTimeTotal";
            this.lblAllTimeTotal.Size = new System.Drawing.Size(14, 15);
            this.lblAllTimeTotal.TabIndex = 8;
            this.lblAllTimeTotal.Text = "0";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(6, 188);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(118, 16);
            this.label13.TabIndex = 2;
            this.label13.Text = "Totally consumed:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(6, 159);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(121, 16);
            this.label11.TabIndex = 2;
            this.label11.Text = "Total consumption:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(76, 159);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 16);
            this.label12.TabIndex = 5;
            this.label12.Text = "1 m/s";
            // 
            // frmTask2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 543);
            this.Controls.Add(this.gbConsumption);
            this.Controls.Add(this.gbAutomation);
            this.Controls.Add(this.gbTimeControl);
            this.Controls.Add(this.gbScheme);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1200, 582);
            this.MinimumSize = new System.Drawing.Size(1200, 582);
            this.Name = "frmTask2";
            this.Text = "Ülesanne 2";
            this.gbScheme.ResumeLayout(false);
            this.gbTimeControl.ResumeLayout(false);
            this.gbTimeControl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbTime)).EndInit();
            this.gbAutomation.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.gbConsumption.ResumeLayout(false);
            this.gbConsumption.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbScheme;
        private System.Windows.Forms.GroupBox gbTimeControl;
        private System.Windows.Forms.GroupBox gbAutomation;
        private System.Windows.Forms.ToolTip toolTipScheme;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lblTimeLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar tbTime;
        private System.Windows.Forms.Timer timerMain;
        private System.Windows.Forms.Label lblSpeed;
        private System.Windows.Forms.Button tbSetSpeed;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbCustomSpeed;
        private DeviceButton dbtnLighting;
        private DeviceButton dbtnRadiator;
        private DeviceButton dbtnFridge;
        private DeviceButton dbtnCoffee;
        private DeviceButton dbtnHeater;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn Device;
        private System.Windows.Forms.DataGridViewTextBoxColumn Consumption;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpLighting;
        private System.Windows.Forms.DateTimePicker dtpRadiator;
        private System.Windows.Forms.DateTimePicker dtpCoffee;
        private System.Windows.Forms.DateTimePicker dtpFridge;
        private System.Windows.Forms.DateTimePicker dtpHeater;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox gbConsumption;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeviceName;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeviceCurrentConsumption;
        private System.Windows.Forms.DataGridViewCheckBoxColumn DeviceIsOn;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblAllTimeTotal;
        private System.Windows.Forms.Label label13;
    }
}