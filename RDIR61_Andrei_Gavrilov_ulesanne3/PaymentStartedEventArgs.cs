﻿using RDIR61_Andrei_Gavrilov_ulesanne3.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDIR61_Andrei_Gavrilov_ulesanne3
{
    public class PaymentStartedEventArgs
    {
        private int number;

        //Did not implement a "Set" so that the only way to give it the Text value is in the constructor
        public int Number
        {
            get { return number; }
        }
        private Receipt receipt;
        //Did not implement a "Set" so that the only way to give it the Text value is in the constructor
        public Receipt Receipt
        {
            get { return receipt; }
        }

        public PaymentStartedEventArgs(Receipt receipt,int number)
            : base()
        {
            this.number = number;
            this.receipt = receipt;
        }
    }
}
