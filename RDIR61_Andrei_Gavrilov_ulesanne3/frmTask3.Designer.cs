﻿namespace RDIR61_Andrei_Gavrilov_ulesanne3
{
    partial class frmTask3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerMain = new System.Windows.Forms.Timer(this.components);
            this.btnChangePriceSave = new System.Windows.Forms.Button();
            this.tbChangePrice = new System.Windows.Forms.TextBox();
            this.cbFuelPricesChange = new System.Windows.Forms.ComboBox();
            this.btnChangePriceAbort = new System.Windows.Forms.Button();
            this.gbPriceChange = new System.Windows.Forms.GroupBox();
            this.bsFuels = new System.Windows.Forms.BindingSource(this.components);
            this.fuelStationControl1 = new RDIR61_Andrei_Gavrilov_ulesanne3.CustomControls.FuelStationControl();
            this.gbPayment = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.FuelStationNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FuelPriceTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FuelStationConfirmButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.fuelStationControl2 = new RDIR61_Andrei_Gavrilov_ulesanne3.CustomControls.FuelStationControl();
            this.fuelStationControl3 = new RDIR61_Andrei_Gavrilov_ulesanne3.CustomControls.FuelStationControl();
            this.fuelStationControl4 = new RDIR61_Andrei_Gavrilov_ulesanne3.CustomControls.FuelStationControl();
            this.gbPriceChange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsFuels)).BeginInit();
            this.gbPayment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // timerMain
            // 
            this.timerMain.Interval = 1000;
            // 
            // btnChangePriceSave
            // 
            this.btnChangePriceSave.Enabled = false;
            this.btnChangePriceSave.Location = new System.Drawing.Point(9, 85);
            this.btnChangePriceSave.Name = "btnChangePriceSave";
            this.btnChangePriceSave.Size = new System.Drawing.Size(58, 23);
            this.btnChangePriceSave.TabIndex = 3;
            this.btnChangePriceSave.Text = "Save";
            this.btnChangePriceSave.UseVisualStyleBackColor = true;
            this.btnChangePriceSave.Click += new System.EventHandler(this.btnChangePriceSave_Click);
            // 
            // tbChangePrice
            // 
            this.tbChangePrice.Location = new System.Drawing.Point(9, 59);
            this.tbChangePrice.Name = "tbChangePrice";
            this.tbChangePrice.Size = new System.Drawing.Size(122, 20);
            this.tbChangePrice.TabIndex = 4;
            this.tbChangePrice.TextChanged += new System.EventHandler(this.TextBox_TextChanged);
            // 
            // cbFuelPricesChange
            // 
            this.cbFuelPricesChange.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFuelPricesChange.FormattingEnabled = true;
            this.cbFuelPricesChange.Location = new System.Drawing.Point(9, 32);
            this.cbFuelPricesChange.Name = "cbFuelPricesChange";
            this.cbFuelPricesChange.Size = new System.Drawing.Size(122, 21);
            this.cbFuelPricesChange.TabIndex = 6;
            this.cbFuelPricesChange.SelectedIndexChanged += new System.EventHandler(this.cbFuelPricesChange_SelectedIndexChanged);
            // 
            // btnChangePriceAbort
            // 
            this.btnChangePriceAbort.Location = new System.Drawing.Point(73, 85);
            this.btnChangePriceAbort.Name = "btnChangePriceAbort";
            this.btnChangePriceAbort.Size = new System.Drawing.Size(58, 23);
            this.btnChangePriceAbort.TabIndex = 3;
            this.btnChangePriceAbort.Text = "Abort";
            this.btnChangePriceAbort.UseVisualStyleBackColor = true;
            this.btnChangePriceAbort.Click += new System.EventHandler(this.btnChangePriceAbort_Click);
            // 
            // gbPriceChange
            // 
            this.gbPriceChange.Controls.Add(this.cbFuelPricesChange);
            this.gbPriceChange.Controls.Add(this.btnChangePriceSave);
            this.gbPriceChange.Controls.Add(this.btnChangePriceAbort);
            this.gbPriceChange.Controls.Add(this.tbChangePrice);
            this.gbPriceChange.Location = new System.Drawing.Point(544, 12);
            this.gbPriceChange.Name = "gbPriceChange";
            this.gbPriceChange.Size = new System.Drawing.Size(255, 139);
            this.gbPriceChange.TabIndex = 7;
            this.gbPriceChange.TabStop = false;
            this.gbPriceChange.Text = "Price change";
            // 
            // fuelStationControl1
            // 
            this.fuelStationControl1.FuelStation = null;
            this.fuelStationControl1.Location = new System.Drawing.Point(12, 12);
            this.fuelStationControl1.Name = "fuelStationControl1";
            this.fuelStationControl1.Size = new System.Drawing.Size(227, 184);
            this.fuelStationControl1.TabIndex = 9;
            // 
            // gbPayment
            // 
            this.gbPayment.Controls.Add(this.dataGridView1);
            this.gbPayment.Location = new System.Drawing.Point(544, 157);
            this.gbPayment.Name = "gbPayment";
            this.gbPayment.Size = new System.Drawing.Size(255, 226);
            this.gbPayment.TabIndex = 10;
            this.gbPayment.TabStop = false;
            this.gbPayment.Text = "Payment";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FuelStationNumber,
            this.FuelPriceTotal,
            this.FuelStationConfirmButton});
            this.dataGridView1.Location = new System.Drawing.Point(7, 20);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(240, 200);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // FuelStationNumber
            // 
            this.FuelStationNumber.HeaderText = "Station #";
            this.FuelStationNumber.Name = "FuelStationNumber";
            this.FuelStationNumber.ReadOnly = true;
            // 
            // FuelPriceTotal
            // 
            this.FuelPriceTotal.HeaderText = "Total €";
            this.FuelPriceTotal.Name = "FuelPriceTotal";
            this.FuelPriceTotal.ReadOnly = true;
            // 
            // FuelStationConfirmButton
            // 
            this.FuelStationConfirmButton.HeaderText = "Confirm";
            this.FuelStationConfirmButton.Name = "FuelStationConfirmButton";
            this.FuelStationConfirmButton.ReadOnly = true;
            // 
            // fuelStationControl2
            // 
            this.fuelStationControl2.FuelStation = null;
            this.fuelStationControl2.Location = new System.Drawing.Point(245, 12);
            this.fuelStationControl2.Name = "fuelStationControl2";
            this.fuelStationControl2.Size = new System.Drawing.Size(227, 184);
            this.fuelStationControl2.TabIndex = 9;
            this.fuelStationControl2.Load += new System.EventHandler(this.fuelStationControl2_Load);
            // 
            // fuelStationControl3
            // 
            this.fuelStationControl3.FuelStation = null;
            this.fuelStationControl3.Location = new System.Drawing.Point(12, 202);
            this.fuelStationControl3.Name = "fuelStationControl3";
            this.fuelStationControl3.Size = new System.Drawing.Size(227, 184);
            this.fuelStationControl3.TabIndex = 9;
            // 
            // fuelStationControl4
            // 
            this.fuelStationControl4.FuelStation = null;
            this.fuelStationControl4.Location = new System.Drawing.Point(245, 202);
            this.fuelStationControl4.Name = "fuelStationControl4";
            this.fuelStationControl4.Size = new System.Drawing.Size(227, 184);
            this.fuelStationControl4.TabIndex = 9;
            // 
            // frmTask3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(808, 395);
            this.Controls.Add(this.gbPayment);
            this.Controls.Add(this.fuelStationControl4);
            this.Controls.Add(this.fuelStationControl3);
            this.Controls.Add(this.fuelStationControl2);
            this.Controls.Add(this.fuelStationControl1);
            this.Controls.Add(this.gbPriceChange);
            this.Name = "frmTask3";
            this.Text = "frmTast3";
            this.gbPriceChange.ResumeLayout(false);
            this.gbPriceChange.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsFuels)).EndInit();
            this.gbPayment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer timerMain;
        private System.Windows.Forms.Button btnChangePriceSave;
        private System.Windows.Forms.TextBox tbChangePrice;
        private System.Windows.Forms.ComboBox cbFuelPricesChange;
        private System.Windows.Forms.BindingSource bsFuels;
        private System.Windows.Forms.Button btnChangePriceAbort;
        private System.Windows.Forms.GroupBox gbPriceChange;
        private CustomControls.FuelStationControl fuelStationControl1;
        private System.Windows.Forms.GroupBox gbPayment;
        private CustomControls.FuelStationControl fuelStationControl2;
        private CustomControls.FuelStationControl fuelStationControl3;
        private CustomControls.FuelStationControl fuelStationControl4;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn FuelStationNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn FuelPriceTotal;
        private System.Windows.Forms.DataGridViewButtonColumn FuelStationConfirmButton;
    }
}