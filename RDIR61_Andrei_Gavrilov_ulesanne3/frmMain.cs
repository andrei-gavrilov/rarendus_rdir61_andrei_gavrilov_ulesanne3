﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RDIR61_Andrei_Gavrilov_ulesanne3
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void btnTask1_Click(object sender, EventArgs e)
        {
            frmTask1 frmTask1 = new frmTask1();
            frmTask1.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmTask2 frmTask2 = new frmTask2();
            frmTask2.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmTask3 frmTask3 = new frmTask3();
            frmTask3.Show();
        }
    }
}
