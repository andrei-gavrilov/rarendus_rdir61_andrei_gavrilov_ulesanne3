﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDIR61_Andrei_Gavrilov_ulesanne3.Entities
{
    public class FuelStation
    {

        public int FuelStationNumber { get; set; }
        public bool IsWorking { get; set; }
        public bool PaymentIsProcessing { get; set;}
        public List<Fuel> FuelsList { get; set; }
        public Receipt Receipt { get; set; }
        

        public FuelStation()
        {
            IsWorking = false;
            FuelsList = new List<Fuel>();
        }
        public FuelStation(List<Fuel> FuelsList)
        {
            this.IsWorking = false;
            this.FuelsList = FuelsList;
        }
        public FuelStation(int FuelStationNumber, List<Fuel> FuelsList)
        {
            this.FuelStationNumber = FuelStationNumber;
            this.IsWorking = false;
            this.FuelsList = FuelsList;
        }
        public FuelStation(int FuelStationNumber)
        {
            this.FuelsList = new List<Fuel>();
            this.FuelStationNumber = FuelStationNumber;
            this.IsWorking = false;
        }
        public FuelStation(bool IsWorking, List<Fuel> FuelsList)
        {
            this.IsWorking = IsWorking;
            this.FuelsList = FuelsList;
        }
    }
}
