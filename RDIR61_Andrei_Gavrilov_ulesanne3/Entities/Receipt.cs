﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDIR61_Andrei_Gavrilov_ulesanne3.Entities
{
    public class Receipt
    {
        public double PricePerLiter { get; set; }//euros
        public double PriceTotal { get; set; }//euros
        public double Quantity { get; set; }//litres
        public Fuel Fuel { get; set; }

        public Receipt()
        {
            PriceTotal = 0;
            Quantity = 0;
        }
        public Receipt(double pricePerLiter, double quantity, Fuel fuel)
        {
            this.PricePerLiter = pricePerLiter;
            this.Quantity = quantity;
            this.Fuel = fuel;
        }
    }
}
