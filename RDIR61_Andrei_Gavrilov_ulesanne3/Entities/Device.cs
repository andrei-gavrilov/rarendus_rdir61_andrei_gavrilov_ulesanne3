﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDIR61_Andrei_Gavrilov_ulesanne3
{
    public class Device
    {
        public string  Name { get; set; }
        public int Consumption { get; set; }
        public Boolean IsOn { get; set; }
        public DateTime Time { get; set; }
        public Device()
        {

        }
        public Device(string name, int consumption, Boolean isOn)
        {
            this.Name =  name;
            this.Consumption = consumption;
            this.IsOn = isOn;
        }
    }
}
