﻿namespace RDIR61_Andrei_Gavrilov_ulesanne3.Entities
{
    public class Fuel
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public Fuel()
        {

        }
        public Fuel(string Name, double Price)
        {
            this.Name = Name;
            this.Price = Price;
        }
        public override string ToString()
        {
            // Generates the text shown in the combo box
            return Name+", "+Price.ToString("0.##")+" EUR/L";
        }
    }
}
