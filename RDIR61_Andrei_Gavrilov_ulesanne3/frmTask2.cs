﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RDIR61_Andrei_Gavrilov_ulesanne3
{
    public partial class frmTask2 : Form
    {
        private static int speed = 1;
        private static double currentTotalConsumption = 0d;
        private static double allTimeTotalConsumption = 0d;
        private  DateTime currentDateTime;
        private static List<DeviceButton> deviceButtonList;
        private static List<DateTimePicker> deviceDateTimesList;

        public frmTask2()
        {
            InitializeComponent();


            //devices and buttons - setup
            dbtnHeater.Device =new Device("Heater", 8000, true);
            dbtnCoffee.Device =new Device("Coffee machine", 400, true);
            dbtnFridge.Device =new Device("Fridge", 110, true);
            dbtnLighting.Device =new Device("Lighting", 20, true);
            dbtnRadiator.Device =new Device("Radiator", 2000, true);
            deviceButtonList = new List<DeviceButton>
            {
                dbtnHeater,dbtnCoffee,dbtnFridge,dbtnLighting,dbtnRadiator
            };
            deviceDateTimesList = new List<DateTimePicker>
            {
                dtpHeater,dtpCoffee,dtpFridge,dtpLighting,dtpRadiator
            };


            foreach (DeviceButton item in deviceButtonList)
            {
                toolTipScheme.SetToolTip(item, item.Device.Name);
            }

            for (int i = 0; i < deviceButtonList.Count; i++)
            {
                deviceButtonList[i].Device.Time =DateTime.Now.Date+ deviceDateTimesList[i].Value.TimeOfDay;
                //deviceButtonList[i].Device.Time = deviceDateTimesList[i].Value;

                Console.WriteLine(deviceButtonList[i].Device.Time.ToLongDateString());
            }





            //devices and buttons 2 - fixes
            StartScheme();
            lblTotal.Text = currentTotalConsumption.ToString();


            //datagridview
            dataGridView1.CurrentCell = null;
            foreach (DeviceButton item in deviceButtonList)
            {
                dataGridView1.Rows.Add(item.Device.Name,item.Device.IsOn==true?item.Device.Consumption:0,item.Device.IsOn);
            }
            


            //time && timer
            this.currentDateTime = DateTime.Now;
            lblTime.Text = currentDateTime.ToString("H:mm");//H:mm:ss
            timerMain.Start();
        }
        
        private void StartScheme()
        {
            foreach (DeviceButton item in deviceButtonList)
            {
                if (item.Device.IsOn)
                {
                    item.BackColor = Color.DarkGreen;
                    currentTotalConsumption += item.Device.Consumption;
                }
                else
                {
                    item.BackColor = Color.DarkRed;
                }
            }
        }
        

        private void timerMain_Tick(object sender, EventArgs e)
        {
           
            currentDateTime=currentDateTime.AddMinutes(speed);
            lblTime.Text = currentDateTime.ToString("H:mm");//H:mm:ss


            
            for (int i = 0; i < deviceButtonList.Count; i++)
            {
                
                TimeSpan ts = (deviceButtonList[i].Device.Time - currentDateTime);
                
                if (ts.TotalMinutes>=0)//budet
                {
                    Console.WriteLine(deviceButtonList[i].Device.Time.ToString());
                    deviceButtonList[i].Device.Time.AddDays(1);
                }
                else{//proshlo
                    Console.WriteLine(deviceButtonList[i].Device.Name + ", " + ts.TotalMinutes);
                    if (deviceButtonList[i].Device.IsOn==true && ts.TotalMinutes>=-1*speed)
                    {
                        UseDevice(deviceButtonList[i]);
                    }
                    //Console.WriteLine(deviceButtonList[i].Device.Time + ", <, " + ts.TotalMinutes);
                }
            }


            foreach (DeviceButton item in deviceButtonList)
            {
                if (item.Device.IsOn)
                {
                    allTimeTotalConsumption += item.Device.Consumption*(speed/60d);
                }
            }
            if (allTimeTotalConsumption>1000d)
            {
                lblAllTimeTotal.Text = (allTimeTotalConsumption/1000d).ToString("0.##")+" kW";
            }
            else
            {
                lblAllTimeTotal.Text = allTimeTotalConsumption.ToString("0.##")+" W";
            }

            if (currentTotalConsumption > 1000d)
            {
                lblTotal.Text = (currentTotalConsumption / 1000d).ToString("0.##") + " kW";
            }
            else
            {
                lblTotal.Text = currentTotalConsumption.ToString("0.##") + " W";
            }
        }


        private void tbTime_ValueChanged(object sender, EventArgs e)
        {
            switch (tbTime.Value)
            {
                case 0:
                    speed = 1;
                    lblSpeed.Text = "1 m/s";
                    break;
                case 1:
                    speed = 3;
                    lblSpeed.Text = "3 m/s";
                    break;
                case 2:
                    speed = 5;
                    lblSpeed.Text = "5 m/s";
                    break;
                case 3:
                    speed = 60;
                    lblSpeed.Text = "1 h/s";
                    break;
            }
        }

        private void tbSetSpeed_Click(object sender, EventArgs e)
        {
            try
            {
                speed=Int32.Parse(tbCustomSpeed.Text);
                lblSpeed.Text = speed +" m/s";
            }
            catch
            {
                MessageBox.Show("Not a valid value","Error",MessageBoxButtons.OK,MessageBoxIcon.Exclamation,MessageBoxDefaultButton.Button1);
            }
        }
        void DeviceButton_Click(object sender, EventArgs e)
        {
            UseDevice(sender as DeviceButton);
        }
        void UseDevice(DeviceButton item)
        {
            item.Device.IsOn = !item.Device.IsOn;
            int rowIndex = -1;
            DataGridViewRow row = dataGridView1.Rows
                .Cast<DataGridViewRow>()
                .Where(r => r.Cells["DeviceName"].Value.ToString().Equals(item.Device.Name))
                .First();
            rowIndex = row.Index;

            dataGridView1.Rows[rowIndex].Cells[2].Value = item.Device.IsOn;
            dataGridView1.ClearSelection();
            dataGridView1.Rows[rowIndex].Selected = true;

            if (item.Device.IsOn)
            {
                item.BackColor = Color.DarkGreen;
                currentTotalConsumption += item.Device.Consumption;
                dataGridView1.Rows[rowIndex].Cells[1].Value = item.Device.Consumption;
            }
            else
            {
                item.BackColor = Color.DarkRed;
                currentTotalConsumption -= item.Device.Consumption;
                dataGridView1.Rows[rowIndex].Cells[1].Value = 0;
            }

        }

        private void dtpHeater_ValueChanged(object sender, EventArgs e)
        {
           /* if ((currentDateTime- dbtnHeater.Device.Time).TotalHours<)
            {

            }
             = dtpHeater.Value.TimeOfDay;*/


            Console.WriteLine(dbtnHeater.Device.Time);
        }
    }
    
}
