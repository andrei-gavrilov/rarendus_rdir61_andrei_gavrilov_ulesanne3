﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDIR61_Andrei_Gavrilov_ulesanne3.Entities;

namespace RDIR61_Andrei_Gavrilov_ulesanne3.CustomControls
{
    public partial class FuelStationControl : UserControl
    {
        public event EventHandler<PaymentStartedEventArgs> addPaymentToScheduleEvent;
        public static double speed = 0.5d;
        private FuelStation fuelStation;
        public FuelStation FuelStation
        {
            get
            {
                return fuelStation;
            }
            set
            {
                fuelStation = value;
                if (fuelStation!=null)
                {
                    bsFuelStation.DataSource = fuelStation.FuelsList;
                    cbFuelChoice.DataSource = bsFuelStation;
                    gbStation.Text = "Fuel Station #" + fuelStation.FuelStationNumber;
                }
            }
        }
        
        
        public FuelStationControl()
        {
            InitializeComponent();
        }

        public void UpdateFuelPriceComboboxes()
        {
            if (!fuelStation.IsWorking && FuelStation.Receipt==null)
            {
                bsFuelStation.ResetBindings(false);
            }
        }


        private void btnAction_Click(object sender, EventArgs e)
        {
            if (FuelStation.Receipt == null)
            {
                FuelStation.Receipt = new Receipt();
            }
            if (!FuelStation.IsWorking)
            {
                cbFuelChoice.Enabled = false;
                if (FuelStation.Receipt.PriceTotal == 0)
                {
                    FuelStation.IsWorking = true;
                    btnAction.Text = "Stop";
                    btnPay.Visible = false;
                    btnPay.Enabled = true;

                    Fuel fuel = cbFuelChoice.SelectedItem as Fuel;
                    FuelStation.Receipt.Fuel = fuel;
                    FuelStation.Receipt.PricePerLiter = fuel.Price;
                    FuelStation.Receipt.Quantity = 0d;

                    timerFuelStation.Start();
                }
                else if (FuelStation.Receipt.PriceTotal > 0)
                {
                    FuelStation.IsWorking = true;
                    btnAction.Text = "Stop";
                    btnPay.Visible = false;

                    timerFuelStation.Start();
                }
            }
            else if (FuelStation.IsWorking)
            {
                FuelStation.IsWorking = false;
                btnAction.Text = "Continue";
                btnPay.Visible = true;

                timerFuelStation.Stop();
            }
        }

        private void btnPay_Click(object sender, EventArgs e)
        {
            btnAction.Text = "Start";
            btnAction.Enabled = false;

            btnPay.Text = "Processing...";
            btnPay.Enabled = false;

            if (addPaymentToScheduleEvent != null)
            {
                addPaymentToScheduleEvent(sender, new PaymentStartedEventArgs(fuelStation.Receipt,fuelStation.FuelStationNumber));
            }

            //btnPay.Visible = false;
            //lblFill.Text = null;
            //lblPrice.Text = "Succesfull payment";

            //bsFuelStation.ResetBindings(false);
            //FuelStation.Receipt = null;
        }

        public void SoftResetStation()
        {
            cbFuelChoice.Enabled = true;
            FuelStation.Receipt = null;
            lblPrice.Text = "Price";
            lblFill.Text = "Fill";
            btnPay.Text = "Pay";
            btnPay.Visible = false;
            btnAction.Text = "Start";
            btnAction.Enabled = true;
        }

        private void timerFuelStation_Tick(object sender, EventArgs e)
        {
            FuelStation.Receipt.Quantity += speed * timerFuelStation.Interval / 1000;
            lblFill.Text = FuelStation.Receipt.Quantity.ToString("F") + " litres";

            FuelStation.Receipt.PriceTotal += speed*FuelStation.Receipt.PricePerLiter * timerFuelStation.Interval / 1000;
            lblPrice.Text = FuelStation.Receipt.PriceTotal.ToString("F") + " euros";
        }
    }
}
