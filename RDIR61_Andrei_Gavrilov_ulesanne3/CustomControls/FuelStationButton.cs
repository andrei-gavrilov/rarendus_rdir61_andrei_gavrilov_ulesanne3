﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RDIR61_Andrei_Gavrilov_ulesanne3.Entities
{
    public partial class FuelStationButton : Button
    {
        public FuelStation FuelStation { get; set; }
        public FuelStationButton()
        {
            InitializeComponent();
        }
        public FuelStationButton(FuelStation FuelStation)
        {
            this.FuelStation = FuelStation;
            InitializeComponent();
        }
        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }

        private void FuelStationButton_Click(object sender, EventArgs e)
        {

        }
    }
}
