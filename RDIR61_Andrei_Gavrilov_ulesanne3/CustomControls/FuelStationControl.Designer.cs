﻿namespace RDIR61_Andrei_Gavrilov_ulesanne3.CustomControls
{
    partial class FuelStationControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gbStation = new System.Windows.Forms.GroupBox();
            this.btnPay = new System.Windows.Forms.Button();
            this.btnAction = new System.Windows.Forms.Button();
            this.lblPrice = new System.Windows.Forms.Label();
            this.cbFuelChoice = new System.Windows.Forms.ComboBox();
            this.lblFill = new System.Windows.Forms.Label();
            this.timerFuelStation = new System.Windows.Forms.Timer(this.components);
            this.bsFuelStation = new System.Windows.Forms.BindingSource(this.components);
            this.gbStation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsFuelStation)).BeginInit();
            this.SuspendLayout();
            // 
            // gbStation
            // 
            this.gbStation.Controls.Add(this.btnPay);
            this.gbStation.Controls.Add(this.btnAction);
            this.gbStation.Controls.Add(this.lblPrice);
            this.gbStation.Controls.Add(this.cbFuelChoice);
            this.gbStation.Controls.Add(this.lblFill);
            this.gbStation.Location = new System.Drawing.Point(3, 3);
            this.gbStation.Name = "gbStation";
            this.gbStation.Size = new System.Drawing.Size(217, 174);
            this.gbStation.TabIndex = 9;
            this.gbStation.TabStop = false;
            this.gbStation.Text = "Fuel station #";
            // 
            // btnPay
            // 
            this.btnPay.Location = new System.Drawing.Point(125, 103);
            this.btnPay.Name = "btnPay";
            this.btnPay.Size = new System.Drawing.Size(75, 23);
            this.btnPay.TabIndex = 0;
            this.btnPay.Text = "Pay";
            this.btnPay.UseVisualStyleBackColor = true;
            this.btnPay.Visible = false;
            this.btnPay.Click += new System.EventHandler(this.btnPay_Click);
            // 
            // btnAction
            // 
            this.btnAction.Location = new System.Drawing.Point(21, 66);
            this.btnAction.Name = "btnAction";
            this.btnAction.Size = new System.Drawing.Size(75, 23);
            this.btnAction.TabIndex = 0;
            this.btnAction.Text = "Start";
            this.btnAction.UseVisualStyleBackColor = true;
            this.btnAction.Click += new System.EventHandler(this.btnAction_Click);
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Location = new System.Drawing.Point(18, 108);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(31, 13);
            this.lblPrice.TabIndex = 1;
            this.lblPrice.Text = "Price";
            // 
            // cbFuelChoice
            // 
            this.cbFuelChoice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFuelChoice.FormattingEnabled = true;
            this.cbFuelChoice.Location = new System.Drawing.Point(21, 32);
            this.cbFuelChoice.Name = "cbFuelChoice";
            this.cbFuelChoice.Size = new System.Drawing.Size(115, 21);
            this.cbFuelChoice.TabIndex = 2;
            // 
            // lblFill
            // 
            this.lblFill.AutoSize = true;
            this.lblFill.Location = new System.Drawing.Point(18, 130);
            this.lblFill.Name = "lblFill";
            this.lblFill.Size = new System.Drawing.Size(19, 13);
            this.lblFill.TabIndex = 1;
            this.lblFill.Text = "Fill";
            // 
            // timerFuelStation
            // 
            this.timerFuelStation.Tick += new System.EventHandler(this.timerFuelStation_Tick);
            // 
            // FuelStationControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gbStation);
            this.Name = "FuelStationControl";
            this.Size = new System.Drawing.Size(234, 187);
            this.gbStation.ResumeLayout(false);
            this.gbStation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsFuelStation)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbStation;
        private System.Windows.Forms.Button btnPay;
        private System.Windows.Forms.Button btnAction;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.ComboBox cbFuelChoice;
        private System.Windows.Forms.Label lblFill;
        private System.Windows.Forms.Timer timerFuelStation;
        public System.Windows.Forms.BindingSource bsFuelStation;
    }
}
