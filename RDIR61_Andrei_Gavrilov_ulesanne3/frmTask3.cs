﻿using RDIR61_Andrei_Gavrilov_ulesanne3.CustomControls;
using RDIR61_Andrei_Gavrilov_ulesanne3.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RDIR61_Andrei_Gavrilov_ulesanne3
{
    public partial class frmTask3 : Form
    {
        List<FuelStationControl> fuelStationControlList;
        List<Fuel> fuelList = new List<Fuel> {
            new Fuel("Fuel1", 1.00d),
            new Fuel("Fuel2", 2.00d),
            new Fuel("Fuel3", 3.00d)
        };


        public frmTask3()
        {
            InitializeComponent();

            //fuels and their prices
            bsFuels.DataSource = fuelList;
            cbFuelPricesChange.DataSource = bsFuels;

            fuelStationControlList = new List<FuelStationControl>
            {
                fuelStationControl1,fuelStationControl2,fuelStationControl3,fuelStationControl4
            };

            for (int i = 0; i < fuelStationControlList.Count; i++)
            {
                fuelStationControlList[i].FuelStation = new FuelStation(i+1,fuelList);
                fuelStationControlList[i].addPaymentToScheduleEvent += new EventHandler<PaymentStartedEventArgs>(log_addPaymentToDataGridView);
            }

            //fuelStationControlList[i].FuelStation = new FuelStation(i + 1);
            //fuelStationControlList[0].FuelStation.FuelsList.Add(fuelList[0]);
            //fuelStationControlList[1].FuelStation.FuelsList.Add(fuelList[1]);
            //fuelStationControlList[2].FuelStation.FuelsList.Add(fuelList[2]);
        }


        private void btnChangePriceSave_Click(object sender, EventArgs e)
        {
            try
            {
                Fuel fuel = cbFuelPricesChange.SelectedItem as Fuel;
                fuel.Price = Double.Parse(tbChangePrice.Text);
                bsFuels.ResetBindings(false);
                foreach (FuelStationControl item in fuelStationControlList)
                {
                    item.UpdateFuelPriceComboboxes();
                }
            }
            catch
            {
                MessageBox.Show("Invalid price");
            }
        }

        private void btnChangePriceAbort_Click(object sender, EventArgs e)
        {
            tbChangePrice.Text = null;
        }
        
        
        private void TextBox_TextChanged(object sender, EventArgs e)
        {
                // Convert the text to a Double and determine if it is a negative number.
                if (tbChangePrice.Text=="")
                {
                    btnChangePriceSave.Enabled = false;
                }
                else
                {
                    btnChangePriceSave.Enabled = true;
                }
        }

        private void cbFuel1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        

        private void gbStation1_Enter(object sender, EventArgs e)
        {

        }
        private void log_addPaymentToDataGridView(object sender, PaymentStartedEventArgs e)
        {
            //dataGridView1.Rows.Add(e.Receipt);
            dataGridView1.Rows.Add(e.Number, e.Receipt.PriceTotal,"Confirm");
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            if (dataGridView1.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "Payed";
                int stationId= (int)dataGridView1.Rows[e.RowIndex].Cells[0].Value;
                fuelStationControlList.Find(x => x.FuelStation.FuelStationNumber == stationId).SoftResetStation();
            }
        }

        private void fuelStationControl2_Load(object sender, EventArgs e)
        {

        }

        private void cbFuelPricesChange_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
